<?php

/**
 * @file
 * Define JivoSite URLs.
 */

define('JIVOSITE_INSTALL_URL', 'https://admin.jivosite.com/integration/install');
define('JIVOSITE_LOGIN_URL', 'https://admin.jivosite.com/integration/login');

/**
 * Partner ID.
 */
define('JIVOSITE_DEFAULT_PARTNER_ID', 'drupal_module');

/**
 * Implements hook_menu().
 */
function jivosite_menu() {
  $items = array();
  $items['admin/config/services/jivosite'] = array(
    'title' => 'JivoSite',
    'description' => 'JivoSite Settings',
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jivosite_settings_install_form'),
    'file' => 'jivosite.admin.inc',
  );

  $items['admin/config/services/jivosite/signup'] = array(
    'title' => 'Signup',
    'description' => 'Signup JivoSite',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );

  $items['admin/config/services/jivosite/desktop'] = array(
    'title' => 'Install Desktop App',
    'description' => 'Install Desktop App',
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jivosite_settings_app_form'),
    'file' => 'jivosite.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  $items['admin/config/services/jivosite/settings'] = array(
    'title' => 'Settings',
    'description' => 'JivoSite settings',
    'access arguments' => array('administer site configuration'),
    'page callback' => 'jivosite_settings_page',
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );

  return $items;
}

/**
 * Page callback. Returns main signup page.
 */
function jivosite_settings_page() {
  return theme('jivosite_settings_page');
}

/**
 * Implements hook_init().
 */
function jivosite_init() {

  $paths = trim(_jivosite_settings('settings', 'paths'));
  $id = _jivosite_settings('install', 'widgetID');

  if ($paths) {
    $page_match = FALSE;
    $pages = drupal_strtolower($paths);
    $visibility = _jivosite_settings('settings', 'visibility');

    // Convert the Drupal path to lowercase.
    $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
    // Compare the lowercase internal and lowercase path alias (if any).
    $page_match = drupal_match_path($path, $pages);

    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
    }
    $page_match = !($visibility xor $page_match);
  }
  else {
    $page_match = TRUE;
  }

  if ($page_match && $id) {
    jivosite_js_widget($id);
  }
}

/**
 * Implements hook_theme().
 */
function jivosite_theme() {
  return array(
    'jivosite_button_chat' => array(
      'variables' => array(),
    ),

    'jivosite_button_call' => array(
      'variables' => array(),
    ),

    'jivosite_desktop_app' => array(
      'variables' => array(),
    ),

    'jivosite_settings_page' => array(
      'variables' => array(),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function jivosite_block_info() {
  $blocks = array();
  $blocks['button_chat'] = array('info' => t('JivoSite chat button'));
  $blocks['button_call'] = array('info' => t('JivoSite call button'));

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function jivosite_block_view($delta = '') {
  $block = array();
  if ($delta == 'button_chat') {
    $block['subject'] = t('JivoSite chat button');
    $block['content'] = theme('jivosite_button_chat', array());
  }

  if ($delta == 'button_call') {
    $block['subject'] = t('JivoSite call button');
    $block['content'] = theme('jivosite_button_call', array());
  }

  return $block;
}

/**
 * Returns custom chat button.
 */
function theme_jivosite_button_chat($variables) {
  return filter_xss_admin(_jivosite_settings('settings', 'chat'));
}

/**
 * Returns custom call button.
 */
function theme_jivosite_button_call($variables) {
  return filter_xss_admin(_jivosite_settings('settings', 'call'));
}

/**
 * Returns iframe to install desktop app.
 */
function theme_jivosite_desktop_app($variables) {
  $output = '<div style="float:left;margin-right: 1em;"><iframe src="http://www.jivosite.com/install.html" width="217px" height="180px" bgcolor="white" frameborder="0" style="border: none"></iframe></div>';
  $output .= '<div>' . t('This application is a chat client that enables your agents to chat with vistiors in real time. It works on Windows and Mac and provides convenient interface to serve several simultaneous chats and quick-answer new incoming chats.') . '</div>';

  return $output;
}

/**
 * Returns JivoSite's logo.
 */
function _jivosite_logo() {
  $variables = array(
    'path' => drupal_get_path('module', 'jivosite') . '/style/jivosite.png',
    'alt' => t('JivoSite'),
    'title' => t('JivoSite'),
    'width' => 189,
    'height' => 91,
    'attributes' => array('class' => 'jivosite-logo'),
  );

  return '<div class="logo">' . theme('image', $variables) . '</div>';
}

/**
 * Theme function. Returns main signup form.
 */
function theme_jivosite_settings_page($variables) {

  $output = '';
  include_once 'jivosite.admin.inc';

  $output .= '<div>' . _jivosite_logo() . '</div>';

  if (_jivosite_settings('install', 'widgetID')) {
    $login_form = drupal_get_form('jivosite_settings_login_form');
    $output .= drupal_render($login_form);

    $output .= '<div style="margin-bottom: 1em;">' . t('Once you click on the button you will be redirected to your JivoSite account, where you can configure your JivoSite widget.', array(
      '@url' => 'https://admin.jivosite.com/widgets/installcode',
    )) . '</div>';
  }

  $form = drupal_get_form('jivosite_settings_form');
  $output .= drupal_render($form);

  return $output;
}

/**
 * Adds required widget JS.
 */
function jivosite_js_widget($id) {
  $js = "
    (function(){ 
      var widget_id = '" . $id . "';
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = '//code.jivosite.com/script/widget/'+widget_id;
      var ss = document.getElementsByTagName('script')[0];
      ss.parentNode.insertBefore(s, ss);
    })();
  ";
  drupal_add_js($js, array(
    'type' => 'inline',
    'scope' => 'footer',
    'weight' => 99,
  ));
}

/**
 * Returns module settings.
 */
function _jivosite_settings($type, $id) {
  $settings = variable_get('jivosite_settings', array());

  return isset($settings[$type][$id]) ? $settings[$type][$id] : NULL;
}

/**
 * Returns common HTTP codes.
 */
function jivosite_http_codes() {
  return array(
    100 => 'Continue',
    101 => 'Switching Protocols',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    307 => 'Temporary Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Time-out',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Large',
    415 => 'Unsupported Media Type',
    416 => 'Requested range not satisfiable',
    417 => 'Expectation Failed',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Time-out',
    505 => 'HTTP Version not supported',
  );
}
