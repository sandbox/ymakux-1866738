-- SUMMARY --

JivoSite livechat is simple and elegant communication solution for your
e-commerce site. It resembles Facebook chat window that is quite familiar
nowadays. Also chat window always stays on top of your web site and does
not blink and reload upon navigation. It will not get buried under dozens
of opened browser windows and provide smooth user experience. If nobody is
online visitor will see offline email form.

The Jivosite module integrates Jivosite live support widget
into your Drupal site.

For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/ymakux/1866738

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/1866738


-- REQUIREMENTS --

filter module.


-- INSTALLATION --

1) Download and enable this module
2) Go to admin/config/services/jivosite and fill out all required fields.
3) After you press "Save" button, a new Jivosite account and widget will be
created for you.
4) Go to admin/config/services/jivosite/login and click "Login" button to
login your Jivosite account. There you can configure various settings
of your live chat.
